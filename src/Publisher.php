<?php

namespace LHGroup\From1cToWeb;

use LHGroup\From1cToWeb\Helper\ConnectionTrait;
use LHGroup\From1cToWeb\Helper\QueueTrait;
use LHGroup\From1cToWeb\Notify\NotifyInterface;
use LHGroup\From1cToWeb\Notify\NullNotify;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Publisher
{
    use QueueTrait;
    use ConnectionTrait;

    protected $connection;
    protected $notifier;

    public function __construct(
        ConnectionConfig $config,
        NotifyInterface $notifier = null
    )
    {
        $this->connection = $this->createConnection($config);

        $this->notifier = $notifier;

        if (null === $notifier) {
            $this->notifier = new NullNotify();
        }
    }

    public function publishMessage(string $queue, string $message)
    {

        $this->eventHappened('Trying connect to queue: ' . $queue);

        $channel = $this->connection->channel();
        $this->runQueue($channel, $queue);
        $this->eventHappened('Connection ok!', NotifyInterface::SUCCESS_MESSAGE);
        $this->sendMessage($message, $queue, $channel);

        $this->eventHappened("\n ============================== \n  Message sent", NotifyInterface::SUCCESS_MESSAGE);

        $channel->close();
        $this->connection->close();

        $this->eventHappened("Connection closed!", NotifyInterface::SUCCESS_MESSAGE);

    }

    protected function sendMessage(string $messageBody, string $queue, AMQPChannel $channel)
    {
        if(mb_strlen($messageBody) > 40000){
            $this->eventHappened("Message is to big to be shown. Sending it \n to queue " . $queue);
        }
        else { $this->eventHappened("Sending message: \n " . $messageBody . " \n to queue " . $queue); }
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));

        $channel->basic_publish($message, '',  $queue);
    }

    protected function eventHappened(string $message, string $type = '')
    {
        $this->notifier->notifyEvent($message, $type);
    }

}