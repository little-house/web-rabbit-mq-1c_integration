<?php

namespace LHGroup\From1cToWeb;

use LHGroup\From1cToWeb\ConnectionConfig;
use LHGroup\From1cToWeb\Exceptions\InvalidProductException;
use LHGroup\From1cToWeb\Exceptions\SomeoneConsumingQueueException;
use LHGroup\From1cToWeb\Exceptions\WrongMessageException;
use LHGroup\From1cToWeb\Helper\ConnectionTrait;
use LHGroup\From1cToWeb\Helper\NotifyTrait;
use LHGroup\From1cToWeb\Helper\QueueTrait;
use LHGroup\From1cToWeb\Item\ItemInterface;
use LHGroup\From1cToWeb\Notify\NotifyInterface;
use LHGroup\From1cToWeb\Queue\QueueInterface;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Wire\GenericContent;
use Tymosh\ErpExchangeReader\Interfaces\ItemFactoryInterface;
use Tymosh\ErpExchangeReader\Reader;


/**
 * Class AbstractQueue
 * @package LHGroup\From1cToWeb\Queue
 * @see example here https://github.com/php-amqplib/php-amqplib/tree/master/demo
 * and here https://habrahabr.ru/post/150134/
 * We using a direct exchange.
 * The routing algorithm behind a direct exchange is simple
 * a message goes to the queues whose binding key exactly matches the routing key of the message. see self::AMPQ_BINDING_ROUTING_KEY
 */
class QueueConsumer
{
    use QueueTrait;
    use ConnectionTrait;
    use NotifyTrait;

    protected $connection;
    protected $notifier;
    protected $channel;
    protected $queue;
    protected $connectionStatus = true;

    const QUEUE_TIMEOUT_SEC = 10;

    public function __construct(
        ConnectionConfig $config,
        NotifyInterface $notifier,
        QueueInterface $queue
    )
    {
        $this->notifier = $notifier;
        $this->connection = $this->createConnection($config);
        $this->channel = $this->connection->channel();
        $this->queue = $queue;
    }


    protected function getQueueName(): string{
        return $this->queue->getQueueName();
    }

    /**
     * @param bool $runForever if you wish to take only 1 message and close connection
     * @throws QueueAlreadyRunningException|SomeoneConsumingQueueException
     */
    public function run(bool $runForever = false, string $consumer = 'default_consumer')
    {

        $this->eventHappened('Connection ok. Channel ok. Trying to declare queue', $this->notifier::SUCCESS_MESSAGE);

        $declaretedQueue = $this->runQueue($this->channel, $this->getQueueName());

        // only one consumer for this queue allowed
        $this->queueHasConsumer($declaretedQueue);

        //no more then one message at a time for 1 consumer
        $this->channel->basic_qos(null, 1, null);

        $this->channel->basic_consume(
            $this->getQueueName(), $consumer, false, false, false, false,
            array($this, 'processMessage')
        );

        $this->eventHappened("Queue declared. Consumer: `" . $consumer . "`. Queue `" . $this->getQueueName() . "`. Consuming..", $this->notifier::SUCCESS_MESSAGE);


        $timeout = self::QUEUE_TIMEOUT_SEC;
        if (true === $runForever) {
            $this->eventHappened("This queue set as permanent and won`t finish till you end it with `quit` message or with rabbitMQ directly");
            $timeout = 0;
        } else {
            $this->eventHappened("The system will listen to this queue for $timeout seconds, after which it will close the channel .");
        }

        try {
            while (count($this->channel->callbacks)) {
                //'Waiting for incoming messages';
                $this->channel->wait(null, false, $timeout);
            }
        }
        catch (AMQPTimeoutException $e){

        }

        $this->eventHappened("\n ============================== \n Queue have been processed ok. Shutting down. Use `runForever` option to run consuming permanently.", $this->notifier::SUCCESS_MESSAGE);
        $this->shutdownConnection();
    }


    public function processMessage(\PhpAmqpLib\Message\AMQPMessage $message)
    {
        $this->eventHappened("\n New message have been consumed", $this->notifier::SUCCESS_MESSAGE);
        if ($message->body === 'quit') {
            $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
            $this->markMessageAsProcessed($message);

            $this->eventHappened("Quit message recived. quiting..", $this->notifier::SUCCESS_MESSAGE);
            $this->shutdownConnection();
            return;
        }

        try {
            $this->checkConsumedMessage($message);
            $this->messageIsFineConsumeIt($message);
        } catch (WrongMessageException $e) {
            $this->errorHappened($e);
        }

        $this->markMessageAsProcessed($message);

    }


    protected function shutdownConnection()
    {
        if($this->connectionStatus === true) {
            $this->eventHappened("Closing current connection. Quiting.");
            $this->channel->close();
            $this->connection->close();
            $this->connectionStatus = false;
        }
    }

    protected function queueHasConsumer($declaretedQueue)
    {
       // var_dump($declaretedQueue);
        if ($declaretedQueue[2] > 0) {
            $this->eventHappened("Somebody is already consuming this queue. Only one consumer allowed");
            $this->shutdownConnection();
            throw new SomeoneConsumingQueueException("Somebody is already consuming this queue. Only one consumer allowed. Try to stop by rabbitMQ directly or send `quit` message to queue " . $this->getQueueName());
        }
    }


    protected function messageIsFineConsumeIt(GenericContent $message)
    {
         $this->queue->processMessage($message->getBody(),$this->notifier);
    }

    //release queue, mark message as success delivered
    protected function markMessageAsProcessed(GenericContent $message)
    {
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    }

    protected function checkConsumedMessage(GenericContent $message)
    {

        $xmlstr = $message->getBody();
        libxml_use_internal_errors(true);

        $doc = simplexml_load_string($xmlstr);
        //$xml = explode("\n", $xmlstr);

        if (false === $doc) {
            throw new WrongMessageException("Your message Contains invalid xml.  Marking message as processed. \r\n Message: \r\n " . $message->getBody() . "");
            /* $errors = libxml_get_errors();

             foreach ($errors as $error) {
                 echo display_xml_error($error, $xml);
             }

             libxml_clear_errors();
            */
        }
    }

    public function __destruct() {
        $this->shutdownConnection();
    }

}