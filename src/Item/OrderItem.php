<?php

namespace LHGroup\From1cToWeb\Item;

use Doctrine\Common\Collections\ArrayCollection;
use LHGroup\From1cToWeb\Item\Order\Item;
use LHGroup\From1cToWeb\Item\Order\Status;
use LHGroup\From1cToWeb\Item\Order\User;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

class OrderItem extends BasikItemAbstract
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $shop_id;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $warehouse;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $date;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $currency;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("float")
     */
    protected $total_price;

    /**
     * @JMS\Type("string")
     */
    protected $delivery;

    /**
     * @JMS\Type("string")
     */
    protected $payment;

    /**
     * @JMS\Type("string")
     */
    protected $comment;


    /**
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Order\Item>")
     * @JMS\XmlList(entry="item")
     */
    protected $items;

    /**
     * @Assert\Type(type="LHGroup\From1cToWeb\Item\Order\Status")
     * @JMS\Type("LHGroup\From1cToWeb\Item\Order\Status")
     */
    protected $status;


    /**
     * @Assert\Type(type="LHGroup\From1cToWeb\Item\Order\User")
     * @JMS\Type("LHGroup\From1cToWeb\Item\Order\User")
     */
    protected $user;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }


    public function getShopId()
    {
        return $this->shop_id;
    }


    public function setShopId($shop_id)
    {
        $this->shop_id = $shop_id;
    }

    public function getWarehouse()
    {
        return $this->warehouse;
    }


    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
    }


    public function getDate()
    {
        return $this->date;
    }


    public function setDate($date)
    {
        $this->date = $date;
    }


    public function getCurrency()
    {
        return $this->currency;
    }


    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }


    public function getTotalPrice()
    {
        return $this->total_price;
    }


    public function setTotalPrice($total_price)
    {
        $this->total_price = $total_price;
    }


    public function getDelivery()
    {
        return $this->delivery;
    }


    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }


    public function getPayment()
    {
        return $this->payment;
    }


    public function setPayment($payment)
    {
        $this->payment = $payment;
    }


    public function getComment()
    {
        return $this->comment;
    }


    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }


    public function setItems(array $items)
    {
        foreach ($items as $item){
            $this->addItem($item);
        }
    }


    public function addItem(Item $item){
        $this->items->add($item);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(Status $status)
    {
        $this->status = $status;
    }


    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }


}