<?php

namespace LHGroup\From1cToWeb\Item\Order;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Item
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $ean;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;


    /**
     * @JMS\Type("integer")
     * @Assert\NotBlank()
     */
    protected $quantity;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("float")
     */
    protected $price;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("float")
     */
    protected $total_price;


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


    public function getEan()
    {
        return $this->ean;
    }


    public function setEan($ean)
    {
        $this->ean = $ean;
    }


    public function getQuantity()
    {
        return $this->quantity;
    }


    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    public function getPrice()
    {
        return $this->price;
    }


    public function setPrice($price)
    {
        $this->price = $price;
    }


    public function getTotalPrice()
    {
        return $this->total_price;
    }


    public function setTotalPrice($total_price)
    {
        $this->total_price = $total_price;
    }



}