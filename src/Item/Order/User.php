<?php

namespace LHGroup\From1cToWeb\Item\Order;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

class User
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;

    /**
     * @JMS\Type("string")
     */
    protected $email;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $phone;

    /**
     * @JMS\Type("string")
     */
    protected $role;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getRole()
    {
        return $this->role;
    }


    public function setRole(string $role)
    {
        $this->role = $role;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setEmail(string $email)
    {
        $this->email = $email;
    }


    public function getPhone()
    {
        return $this->phone;
    }


    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

}