<?php
namespace LHGroup\From1cToWeb\Item\Order;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
class Status
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $id_erp;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;


    public function getIdErp()
    {
        return $this->id_erp;
    }


    public function setIdErp($id_erp)
    {
        $this->id_erp = $id_erp;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


}