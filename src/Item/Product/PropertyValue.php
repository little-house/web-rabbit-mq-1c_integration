<?php

namespace LHGroup\From1cToWeb\Item\Product;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class PropertyValue
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $idErp;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;


    public function getIdErp(): string
    {
        return $this->idErp;
    }


    public function setIdErp(string $idErp)
    {
        $this->idErp = $idErp;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }

}