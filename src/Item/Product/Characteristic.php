<?php

namespace LHGroup\From1cToWeb\Item\Product;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Characteristic
{

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $id_erp;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("LHGroup\From1cToWeb\Item\Product\CharacteristicValue")
     */
    protected $value;


    public function __construct()
    {

    }

    public function getIdErp(): string
    {
        return $this->id_erp;
    }


    public function setIdErp(string $id_erp)
    {
        $this->id_erp = $id_erp;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return null|\LHGroup\From1cToWeb\Item\Product\CharacteristicValue
     */
    public function getValue()
    {
        return $this->value;
    }


    public function setValue(CharacteristicValue $value)
    {
        $this->value = $value;
    }

}