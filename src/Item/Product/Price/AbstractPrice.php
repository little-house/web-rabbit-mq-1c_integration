<?php

namespace LHGroup\From1cToWeb\Item\Product\Price;

use LHGroup\From1cToWeb\Exceptions\InvalidProductException;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

abstract class AbstractPrice
{

    const ALLOWED_DISCOUNT_TYPES = ['procent', 'fixed'];

    /**
     * @Assert\NotBlank()
     * @JMS\Type("float")
     */
    public $price;

    /**
     * @JMS\Type("string")
     */
    public $discount;

    /**
     *
     * @JMS\Type("string")
     */
    public $discountType;


    public function getPrice()
    {
        return $this->price;
    }


    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    public function getDiscount()
    {
        return $this->discount;
    }


    public function setDiscount(int $discount)
    {
        $this->discount = $discount;
    }


    public function getDiscountType()
    {
        return $this->discountType;
    }


    public function setDiscountType(string $discountType)
    {
        if(!in_array($discountType, self::ALLOWED_DISCOUNT_TYPES)){
            throw new InvalidProductException("Такой тип скидки не может быть использован ".$discountType);
        }
        $this->disctounType = $disctounType;
    }

}