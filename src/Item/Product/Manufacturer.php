<?php

namespace LHGroup\From1cToWeb\Item\Product;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Manufacturer
{

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $id_erp;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;

    /**
     * @return string
     */
    public function getIdErp(): string
    {
        return $this->id_erp;
    }

    /**
     * @param string $id_erp
     */
    public function setIdErp(string $id_erp)
    {
        $this->id_erp = $id_erp;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}