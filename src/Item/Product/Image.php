<?php

namespace LHGroup\From1cToWeb\Item\Product;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Image
{

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $path;

    /**
     * @JMS\Type("string")
     */
    protected $url;

    public function getPath()
    {
        return $this->path;
    }

    public function setPath(string $path)
    {
        $this->path = $path;
    }


    public function getUrl()
    {
        return $this->url;
    }


    public function setUrl(string $url)
    {
        $this->url = $url;
    }



}