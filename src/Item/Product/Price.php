<?php

namespace LHGroup\From1cToWeb\Item\Product;

use LHGroup\From1cToWeb\Item\Product\Price\PriceRetail;
use LHGroup\From1cToWeb\Item\Product\Price\PriceWholesale;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Price
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    public $stockErpId;

    /**
     *
     * @JMS\Type("LHGroup\From1cToWeb\Item\Product\Price\PriceRetail")
     */
    public $priceRetail;

    /**
     * @JMS\Type("LHGroup\From1cToWeb\Item\Product\Price\PriceWholesale")
     */
    public $priceWholesale;

    /**
     * @JMS\Type("integer")
     * @Assert\NotBlank()
     */
    public $quantity;

    public function getStockErpId():string
    {
        return $this->stockErpId;
    }

    public function setStockErpId(string $stockErpId)
    {
        $this->stockErpId = $stockErpId;
    }


    public function getPriceRetail():PriceRetail
    {
        return $this->priceRetail;
    }


    public function setPriceRetail(PriceRetail $priceRetail)
    {
        $this->priceRetail = $priceRetail;
    }


    public function getPriceWholesale()
    {
        return $this->priceWholesale;
    }


    public function setPriceWholesale(PriceWholesale $priceWholesale)
    {
        $this->priceWholesale = $priceWholesale;
    }


    public function getQuantity():int
    {
        return $this->quantity;
    }


    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

}