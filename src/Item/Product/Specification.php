<?php

namespace LHGroup\From1cToWeb\Item\Product;

use Doctrine\Common\Collections\ArrayCollection;
use LHGroup\From1cToWeb\Item\Product\Price\AbstractPrice;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Specification
{

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $idErp;
    /**
     * @JMS\Type("string")
     */
    protected $sku;
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $ean;

    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(1)
     * @JMS\Type("float")
     */
    protected $priceBase;
    /**
     * @JMS\Type("string")
     */
    protected $priceDiscount;
    /**
     * @JMS\Type("string")
     */
    protected $priceDiscountType;
    /**
     * @JMS\Type("float")
     */
    protected $priceErp;

    /**
     *
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Price>")
     * @JMS\XmlList(entry="price")
     */
    protected $prices;

    /**
     *
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Characteristic>")
     * @JMS\XmlList(entry="characteristic")
     */
    protected $characteristics;

    /**
     *
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Image>")
     * @JMS\XmlList(entry="image")
     */
    protected $images;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->characteristics = new ArrayCollection();
        $this->images = new ArrayCollection();

    }

    public function getIdErp(): string
    {
        return $this->idErp;
    }


    public function setIdErp(string $idErp)
    {
        $this->idErp = $idErp;
    }


    public function getSku()
    {
        return $this->sku;
    }


    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }


    public function getEan()
    {
        return $this->ean;
    }

    public function setEan(string $ean)
    {
        $this->ean = $ean;
    }


    public function getPriceBase()
    {
        return $this->priceBase;
    }


    public function setPriceBase(float $priceBase)
    {
        $this->priceBase = $priceBase;
    }


    public function getPriceDiscount()
    {
        return $this->priceDiscount;
    }


    public function setPriceDiscount(float $priceDiscount)
    {
        $this->priceDiscount = $priceDiscount;
    }


    public function getPriceErp()
    {
        return $this->priceErp;
    }


    public function setPriceErp(float $priceErp)
    {
        $this->priceErp = $priceErp;
    }


    /**
     * @return Price[]
     */
    public function getPrices(): ArrayCollection
    {
        return $this->prices;
    }


    public function addPrice(Price $prices)
    {
        if (!$this->prices->contains($price)) {
            $this->prices->add($price);
        }
    }

    /**
     * @return Characteristic[]
     */

    public function getCharacteristics(): ArrayCollection
    {
        return $this->characteristics;
    }


    public function addCharacteristics(Characteristic $characteristic)
    {
        if (!$this->characteristics->contains($characteristic)) {
            $this->characteristics->add($characteristic);
        }
    }


    public function setDiscountType(string $discountType)
    {
        if (!in_array($discountType, AbstractPrice::ALLOWED_DISCOUNT_TYPES)) {
            throw new InvalidProductException("Такой тип скидки не может быть использован " . $discountType);
        }
        $this->priceDiscountType = $discountType;
    }


    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        foreach ($images as $image){
            $this->addImage($image);
        }
    }


    public function addImage(Image $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }
    }



}