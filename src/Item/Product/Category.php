<?php

namespace LHGroup\From1cToWeb\Item\Product;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Category
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    public $id_erp;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    public $name;

    /**
     * @JMS\Type("string")
     */
    public $parent;

    /**
     * @return mixed
     */
    public function getIdErp():string
    {
        return $this->id_erp;
    }

    /**
     * @param mixed $id_erp
     */
    public function setIdErp(string $id_erp)
    {
        $this->id_erp = $id_erp;
    }

    /**
     * @return mixed
     */
    public function getName():string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getParent():?string
    {
        return $this->parent;
    }


    public function setParent(string $parent)
    {
        $this->parent = $parent;
    }




}