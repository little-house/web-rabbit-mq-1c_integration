<?php

namespace LHGroup\From1cToWeb\Item;

use Doctrine\Common\Collections\ArrayCollection;
use LHGroup\From1cToWeb\Exceptions\InvalidProductException;
use LHGroup\From1cToWeb\Item\Product\Brand;
use LHGroup\From1cToWeb\Item\Product\Category;
use LHGroup\From1cToWeb\Item\Product\Image;
use LHGroup\From1cToWeb\Item\Product\Manufacturer;
use LHGroup\From1cToWeb\Item\Product\Property;
use LHGroup\From1cToWeb\Item\Product\Specification;
use LHGroup\From1cToWeb\Item\Serializer\ToXml;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

abstract class BasikItemAbstract implements ItemInterface
{
    public function __toString():string
    {
        $output = "";
        ob_start();
        var_dump($this);
        $output .= ob_get_clean();
        return strip_tags($output);
    }

    public function toXML():string
    {
       return ToXml::itemToXml($this);
    }
}