<?php

namespace LHGroup\From1cToWeb\Item\Unserializer;


use LHGroup\From1cToWeb\Exceptions\ItemValidationException;
use LHGroup\From1cToWeb\Helper\NotifyTrait;
use LHGroup\From1cToWeb\Item\ItemInterface;
use LHGroup\From1cToWeb\Item\Validator\ItemValidatorInterface;
use LHGroup\From1cToWeb\Notify\NotifyInterface;
use LHGroup\From1cToWeb\Queue\ProcessorInterface;
use Tymosh\ErpExchangeReader\Interfaces\ItemFactoryInterface;

class XmlItemFactory implements ItemFactoryInterface
{
    use NotifyTrait;

    protected $serializer;
    protected $entityClass;
    protected $notifier;
    protected $validator;
    protected $siteItemProcessor;

    public function __construct(string $entityClass, NotifyInterface $notifier, ItemValidatorInterface $validator, ProcessorInterface $siteItemProcessor)
    {
        $this->serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $this->entityClass = $entityClass;
        $this->notifier = $notifier;
        $this->validator = $validator;
        $this->siteItemProcessor = $siteItemProcessor;
    }

    /**
     * @param $data
     * @return ItemInterface|null
     */
    public function create($data)
    {
        try {
            $this->eventHappened("Unserializing xml item: \n ".$data->asXML().'');
            $item = $this->createItem($data);
            $this->validateItem($item);
            $this->eventHappened("\n Xml item with erp_id `".$item->getIdErp()."` unserialized successfully from xml.", NotifyInterface::SUCCESS_MESSAGE);
            $this->siteItemProcessor->process($item);
            $this->eventHappened("Item `" . $item->getIdErp() . "` has been processed with site processor successfully.", NotifyInterface::SUCCESS_MESSAGE, $item);
            return $item;
        } catch (ItemValidationException $exception) {
            $this->errorHappened($exception);
        } catch (\Throwable $exception) {
            $this->errorHappened($exception);
        }
        return null;
    }

    protected function createItem(\SimpleXMLElement $data): ItemInterface
    {
        return $this->serializer->deserialize($data->asXML(), $this->entityClass, 'xml');
    }

    protected function validateItem(ItemInterface $item)
    {
        $errors = $this->validator->validate($item);
        if ($errors->count() > 0) {
            throw new ItemValidationException("There was validation error during item processing: \n \n ".$errors->__toString());
        }
    }
}