<?php

namespace LHGroup\From1cToWeb\Item;

use Doctrine\Common\Collections\ArrayCollection;
use LHGroup\From1cToWeb\Exceptions\InvalidProductException;
use LHGroup\From1cToWeb\Item\Product\Brand;
use LHGroup\From1cToWeb\Item\Product\Category;
use LHGroup\From1cToWeb\Item\Product\Image;
use LHGroup\From1cToWeb\Item\Product\Manufacturer;
use LHGroup\From1cToWeb\Item\Product\Property;
use LHGroup\From1cToWeb\Item\Product\Specification;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

class ProductItem extends BasikItemAbstract
{

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $id_erp;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $name;


    /**
     * @var Category[]
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Category>")
     * @JMS\XmlList(entry="category")
     */
    protected $categories;

    /**
     * @var Manufacturer
     * @Assert\Type(type="LHGroup\From1cToWeb\Item\Product\Manufacturer")
     * @JMS\Type("LHGroup\From1cToWeb\Item\Product\Manufacturer")
     */
    protected $manufacturer;

    /**
     * @var Manufacturer
     * @Assert\Type(type="LHGroup\From1cToWeb\Item\Product\Brand")
     * @JMS\Type("LHGroup\From1cToWeb\Item\Product\Brand")
     */
    protected $brand;

    /**
     *
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Specification>")
     * @JMS\XmlList(entry="specification")
     */
    public $specifications = [];

    /**
     *
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Property>")
     * @JMS\XmlList(entry="property")
     */
    protected $properties;

    /**
     *
     * @JMS\Type("ArrayCollection<LHGroup\From1cToWeb\Item\Product\Image>")
     * @JMS\XmlList(entry="image")
     */
    protected $images;

    /**
     * @JMS\Type("string")
     */
    protected $width;

    /**
     * @JMS\Type("string")
     */
    protected $height;

    /**
     * @JMS\Type("string")
     */
    protected $depth;

    /**
     * @JMS\Type("string")
     */
    protected $weight;

    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    protected $type;

    const ALLOWED_PRODUCT_TYPES = ['wardrobe'];



    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->specifications = new ArrayCollection();
        $this->properties = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    /**
     * @param Category[] $categories
     */
    public function setCategories(array $categories)
    {
        foreach ($categories as $category){
            $this->addCategory($category);
        }
    }

    /**
     * @param Specification[] $specifications
     */
    public function setSpecifications($specifications)
    {
        foreach ($specifications as $specification){
            $this->addSpecification($specification);
        }
    }

    /**
     * @param Property[] $properties
     */
    public function setProperties($properties)
    {
        foreach ($properties as $property){
            $this->addProperty($property);
        }
    }


    public function setImages($images)
    {
        foreach ($images as $image){
            $this->addImage($image);
        }
    }


    public function getType():string
    {
        return $this->type;
    }


    public function setType(string $type)
    {
        if(!in_array($type,self::ALLOWED_PRODUCT_TYPES)){
            throw new InvalidProductException("Тип продукта ".$type." запрещен!");
        }
        $this->type = $type;
    }



    public function getIdErp(): string
    {
        return $this->id_erp;
    }


    public function setIdErp(string $idErp)
    {
        $this->id_erp = $idErp;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getBrand()
    {
        return $this->brand;
    }


    public function setBrand(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Category[]
     */
    public function getCategories(): ArrayCollection
    {
        return $this->categories;
    }


    public function addCategory(Category $category)
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }
    }


    public function getManufacturer()
    {
        return $this->manufacturer;
    }


    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return Specification[]
     */
    public function getSpecifications(): ArrayCollection
    {
        return $this->specifications;
    }

    public function addSpecification(Specification $specification)
    {
        if (!$this->specifications->contains($specification)) $this->specifications->add($specification);
    }


    /**
     * @return Property[]
     */
    public function getProperties(): ArrayCollection
    {
        return $this->properties;
    }


    public function addProperty(Property $property)
    {
        if (!$this->properties->contains($property)) $this->properties->add($property);
    }


    /**
     * @return Image[]|null
     */
    public function getImages(): ArrayCollection
    {
        return $this->images;
    }

    public function addImage(Image $image)
    {
        if (!$this->images->contains($image)) $this->images->add($image);
    }


    public function getWidth()
    {
        return $this->width;
    }


    public function setWidth(float $width)
    {
        $this->width = $width;
    }


    public function getHeight()
    {
        return $this->height;
    }


    public function setHeight(float $height)
    {
        $this->height = $height;
    }


    public function getDepth()
    {
        return $this->depth;
    }


    public function setDepth(float $depth)
    {
        $this->depth = $depth;
    }


    public function getWeight()
    {
        return $this->weight;
    }


    public function setWeight(float $weight)
    {
        $this->weight = $weight;
    }

}