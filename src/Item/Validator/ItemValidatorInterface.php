<?php

namespace LHGroup\From1cToWeb\Item\Validator;


use LHGroup\From1cToWeb\Item\ItemInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ItemValidatorInterface
{
    public function validate(ItemInterface $item): ConstraintViolationList;
}