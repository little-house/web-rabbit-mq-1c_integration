<?php

namespace LHGroup\From1cToWeb\Item\Validator;


use Doctrine\Common\Collections\ArrayCollection;
use LHGroup\From1cToWeb\Item\ItemInterface;
use LHGroup\From1cToWeb\Item\ProductItem;
use LHGroup\From1cToWeb\Item\Validator\ItemValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;


class ProductValidator extends AbstractValidator
{
    public function validate(ItemInterface $item): ConstraintViolationList
    {
        return $this->validateProduct($item);
    }


    protected function validateProduct(ProductItem $item): ConstraintViolationList
    {
        $constraintViolationList = new ConstraintViolationList();

        $this->validateRelatedEntity($item, $constraintViolationList);
        $this->validateRelatedEntity($item->getManufacturer(), $constraintViolationList);
        $this->validateRelatedEntity($item->getBrand(), $constraintViolationList);
        foreach ($item->getSpecifications() as $specification){
            $this->validateRelatedEntity($specification, $constraintViolationList);
            foreach ($specification->getCharacteristics() as $characteristic){
                $this->validateRelatedEntity($characteristic, $constraintViolationList);
            }
            foreach ($specification->getPrices() as $price){
                $this->validateRelatedEntity($price, $constraintViolationList);
            }
        }

        foreach ($item->getCategories() as $category){
            $this->validateRelatedEntity($category, $constraintViolationList);
        }

        foreach ($item->getProperties() as $property){
            $this->validateRelatedEntity($property, $constraintViolationList);
        }

        return $constraintViolationList;
    }

}