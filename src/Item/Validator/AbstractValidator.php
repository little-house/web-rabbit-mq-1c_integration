<?php

namespace LHGroup\From1cToWeb\Item\Validator;

use Doctrine\Common\Collections\ArrayCollection;
use LHGroup\From1cToWeb\Item\Validator\ItemValidatorInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\RecursiveValidator;

abstract class AbstractValidator implements ItemValidatorInterface
{
    /**
     * @var RecursiveValidator
     */
    protected $validator;

    public function __construct()
    {
        $this->validator = \Symfony\Component\Validator\Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
    }

    protected function validateRelatedEntity($entity, ConstraintViolationList $constraintViolationList)
    {
        $errors = $this->validator->validate($entity);
        if($errors->count() > 0) {
            $constraintViolationList->addAll($errors);
        }

    }
}