<?php
namespace LHGroup\From1cToWeb\Item\Serializer;

use LHGroup\From1cToWeb\Item\ItemInterface;

class ToXml
{
    public static function itemToXml(ItemInterface $item):string{
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $xmlContent = $serializer->serialize($item, 'xml');
        return $xmlContent;
    }
}