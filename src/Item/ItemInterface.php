<?php

namespace LHGroup\From1cToWeb\Item;

interface ItemInterface
{
    public function __toString():string;
    public function toXML():string;
}