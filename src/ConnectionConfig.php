<?php

namespace LHGroup\From1cToWeb;


class ConnectionConfig
{
    protected $host;
    protected $port;
    protected $user;
    protected $pass;
    protected $vhost;
    protected $ampqDebug = false;

    public function __construct(
        string $host,
        string $port,
        string $user,
        string $pass,
        string $vhost
    )
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
        $this->vhost = $vhost;
    }

    public function getHost()
    {
        return $this->host;
    }


    public function getPort()
    {
        return $this->port;
    }


    public function getUser()
    {
        return $this->user;
    }


    public function getPass()
    {
        return $this->pass;
    }


    public function getVhost()
    {
        return $this->vhost;
    }


    public function isAmpqDebug():bool
    {
        return $this->ampqDebug;
    }

    public function setAmpqDebug(bool $ampqDebug)
    {
        $this->ampqDebug = $ampqDebug;
    }

}