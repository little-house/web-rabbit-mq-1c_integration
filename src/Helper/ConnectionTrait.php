<?php
namespace LHGroup\From1cToWeb\Helper;

use LHGroup\From1cToWeb\ConnectionConfig;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

trait ConnectionTrait
{

   protected function createConnection(ConnectionConfig $config):AMQPStreamConnection{
       return new AMQPStreamConnection(
           $config->getHost(),
           $config->getPort(),
           $config->getUser(),
           $config->getPass(),
           $config->getVhost()
       );
   }
}