<?php
namespace LHGroup\From1cToWeb\Helper;

use PhpAmqpLib\Channel\AMQPChannel;

trait QueueTrait
{

    /*
     * Run queue if it is not created yet
     */
    protected function runQueue(AMQPChannel $channel, string $queue):array
    {
        return $channel->queue_declare($queue, false, true, false, false);
    }
}