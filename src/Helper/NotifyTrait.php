<?php

namespace LHGroup\From1cToWeb\Helper;


trait NotifyTrait
{

    protected function errorHappened(\Throwable $e)
    {
        $this->notifier->notifyError($e);
    }


    protected function eventHappened(string $message, $type = 'info', $event = null)
    {
        $this->notifier->notifyEvent($message, $type, $event);
    }
}