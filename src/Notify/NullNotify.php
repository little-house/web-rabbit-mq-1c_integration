<?php

namespace LHGroup\From1cToWeb\Notify;


use LHGroup\From1cToWeb\Notify\NotifyInterface;

class NullNotify implements NotifyInterface {

    public function notifyError(\Throwable $exception){

    }

    public function notifyEvent(string $message, string $type = 'info', $event = null){

    }
}