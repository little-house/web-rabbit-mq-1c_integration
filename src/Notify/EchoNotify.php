<?php

namespace LHGroup\From1cToWeb\Notify;


use LHGroup\From1cToWeb\Notify\NotifyInterface;

class EchoNotify implements NotifyInterface {

    public function notifyError(\Throwable $exception){
        echo "Exception: ".$exception->getMessage()."\r\n";
    }

    public function notifyEvent(string $message, string $type = 'info', $event = null){
        echo "Event: ".$message."\r\n";
    }
}