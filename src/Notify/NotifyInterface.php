<?php

namespace LHGroup\From1cToWeb\Notify;


interface NotifyInterface
{
    const INFO_MESSAGE = 'info';
    const ERROR_MESSAGE = 'info';
    const SUCCESS_MESSAGE = 'success';

    public function notifyError(\Throwable $exception);

    public function notifyEvent(string $message, string $type = 'info', $event = null);

}