<?php

namespace LHGroup\From1cToWeb\Command;

use LHGroup\From1cToWeb\ConnectionConfig;
use LHGroup\From1cToWeb\Notify\ConsoleNotify;
use LHGroup\From1cToWeb\NullProcessor;
use LHGroup\From1cToWeb\ProcessorInterface;
use LHGroup\From1cToWeb\Publisher;
use LHGroup\From1cToWeb\Queue\Product;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PublishCommand extends Command
{
    protected $ampqConfig;

    public function __construct($name = null, ConnectionConfig $ampqConfig)
    {
        parent::__construct($name);
        $this->ampqConfig = $ampqConfig;
    }

    protected function configure()
    {
        $this->setName('integration:publish:message')
            ->setDescription('Publishes a message to the specified queue')
            ->setHelp("
            Sends message to queue.
            You can also send `quit` message to stop 1 consumer from listening queue.
            Here is the aviable queues:
                - " . Product::QUEUE_NAME . "
            ");

        $this->addOption(
            'queue',
            null,
            InputOption::VALUE_REQUIRED,
            'Queue name to publish your message',
            null
        );

        $this->addOption(
            'message',
            null,
            InputOption::VALUE_REQUIRED,
            'Message',
            null
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $input->getOption('queue');
        if (null === $queue) {
            throw new InvalidOptionException("Queue name option can`t be null");
        }

        $message = $input->getOption('message');
        if (null === $message) {
            throw new InvalidOptionException("Message option can`t be null");
        }

        $notifier = new ConsoleNotify($output);
        $publisher = new Publisher($this->ampqConfig, $notifier);
        $publisher->publishMessage($queue, $message);

    }
}