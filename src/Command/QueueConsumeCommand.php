<?php

namespace LHGroup\From1cToWeb\Command;

use LHGroup\From1cToWeb\ConnectionConfig;
use LHGroup\From1cToWeb\Item\ProductItem;
use LHGroup\From1cToWeb\Item\Unserializer\XmlItemFactory;
use LHGroup\From1cToWeb\Item\Validator\ProductValidator;
use LHGroup\From1cToWeb\Notify\ConsoleNotify;
use LHGroup\From1cToWeb\NullProcessor;
use LHGroup\From1cToWeb\ProcessorInterface;
use LHGroup\From1cToWeb\Queue\Update;
use LHGroup\From1cToWeb\QueueConsumer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \LHGroup\From1cToWeb\Queue;
use Tymosh\ErpExchangeReader\Reader;

class QueueConsumeCommand extends Command
{
    protected $ampqConfig;

    public function __construct($name = null, ConnectionConfig $ampqConfig)
    {
        parent::__construct($name);
        $this->ampqConfig = $ampqConfig;
    }

    protected function configure()
    {
        $this->setName('integration:consume:debug')
            ->setDescription('Debug product consuming from specified queue. Allowed queues are: 
            - ' . Queue\Product::QUEUE_NAME . '
            - ' . Queue\Store::QUEUE_NAME)
            ->setHelp('
            This command allows you to debug the proccess with NullProccessor and ConsoleNotify.
            Why this only for debug? Because you need your own processor to process item from rabbitMq');

        $this->addOption(
            'queue',
            null,
            InputOption::VALUE_REQUIRED,
            'Queue to consume',
            null
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $input->getOption('queue');
        if (null === $queue) {
            throw new InvalidOptionException("Queue name option must be set");
        }



        $notifier = new ConsoleNotify($output);


        switch ($queue) {
            case Queue\Product::QUEUE_NAME:
                $itemEntity = ProductItem::class;
                $entityValidator = ProductValidator::class;
                $queue = Queue\Product::class;
                break;
            default:
                throw new InvalidOptionException("Queue name invalid.");
        }

        $itemFactory = new XmlItemFactory(
            $itemEntity,
            $notifier,
            new $entityValidator(),
            new Queue\NullProcessor()
        );

        $reader = new Reader\Xml($itemFactory);

        $queue = new $queue(
            $reader
        );
        $queueConsumer = new QueueConsumer(
            $this->ampqConfig,
            $notifier,
            $queue
        );


        $queueConsumer->run($runForever = false);
    }

}