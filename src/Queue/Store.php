<?php

namespace LHGroup\From1cToWeb\Queue;

use Tymosh\ErpExchangeReader\Interfaces\ItemFactoryInterface;

class Store extends AbstractQueue implements QueueInterface
{
    const QUEUE_NAME = 'stores_from_1c_to_lh';

}