<?php

namespace LHGroup\From1cToWeb\Queue;

use LHGroup\From1cToWeb\Item\ItemInterface;
use LHGroup\From1cToWeb\Item\Serializer\BasicUnserializer;

use LHGroup\From1cToWeb\Notify\NotifyInterface;
use Tymosh\ErpExchangeReader\Interfaces\ItemFactoryInterface;
use Tymosh\ErpExchangeReader\Interfaces\ReaderInterface;

abstract class AbstractQueue implements QueueInterface
{

    protected $reader;

    public function __construct(
        ReaderInterface $reader
    )
    {
        $this->reader = $reader;
    }

    public function processMessage(string $xmlMessage, NotifyInterface $notifier)
    {
        $this->reader->processString($xmlMessage);
    }

    public function getQueueName(): string
    {
        return static::QUEUE_NAME;
    }
}