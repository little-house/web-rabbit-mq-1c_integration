<?php

namespace LHGroup\From1cToWeb\Queue;


use LHGroup\From1cToWeb\Item\ItemInterface;

class NullProcessor implements ProcessorInterface
{

    public function process(ItemInterface $item)
    {

    }
}