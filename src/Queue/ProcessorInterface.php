<?php

namespace LHGroup\From1cToWeb\Queue;


use LHGroup\From1cToWeb\Item\ItemInterface;

interface ProcessorInterface
{
    public function process(ItemInterface $item);
}