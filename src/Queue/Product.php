<?php

namespace LHGroup\From1cToWeb\Queue;

use LHGroup\From1cToWeb\Item\ItemInterface;
use LHGroup\From1cToWeb\Item\Serializer\SerializerInterface;
use LHGroup\From1cToWeb\Item\Serializer\UnserializerInterface;
use Tymosh\ErpExchangeReader\Interfaces\ItemFactoryInterface;

class Product extends AbstractQueue implements QueueInterface
{
    const QUEUE_NAME = 'products_from_1c_to_lh';
}