<?php

namespace LHGroup\From1cToWeb\Queue;


use LHGroup\From1cToWeb\Item\ItemInterface;
use LHGroup\From1cToWeb\Item\Serializer\SerializerInterface;
use LHGroup\From1cToWeb\Notify\NotifyInterface;

interface QueueInterface
{
    public function getQueueName(): string;
    public function processMessage(string $xmlMessage, NotifyInterface $notifier);
}