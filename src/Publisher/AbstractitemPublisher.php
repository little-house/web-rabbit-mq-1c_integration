<?php

namespace LHGroup\From1cToWeb\Publisher;

use LHGroup\From1cToWeb\ConnectionConfig;
use LHGroup\From1cToWeb\Item\OrderItem;
use LHGroup\From1cToWeb\Notify\NotifyInterface;
use LHGroup\From1cToWeb\Publisher;

abstract class AbstractitemPublisher
{
    protected $config;
    protected $notify;

    public function __construct(ConnectionConfig $config, NotifyInterface $notify)
    {
        $this->config = $config;
        $this->notify = $notify;
    }

    protected function send(OrderItem $orderItem, string $queueName)
    {
        $publisher = new Publisher($this->config, $this->notify);
        $publisher->publishMessage($queueName, $orderItem->toXML());
    }

}