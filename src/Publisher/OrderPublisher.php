<?php

namespace LHGroup\From1cToWeb\Publisher;

use LHGroup\From1cToWeb\ConnectionConfig;
use LHGroup\From1cToWeb\Item\OrderItem;
use LHGroup\From1cToWeb\Notify\NotifyInterface;
use LHGroup\From1cToWeb\Publisher;

class OrderPublisher extends AbstractitemPublisher
{
    const ORDERS_FROM_LH_TO_1C = 'orders_from_lh_to_1c';

    public function sendOrder(OrderItem $orderItem)
    {
        return $this->send($orderItem, self::ORDERS_FROM_LH_TO_1C);
    }

}