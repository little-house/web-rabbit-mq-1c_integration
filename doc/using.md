# Использование
Самый простой и правильный вариант использования скрипта - это его запуск через консоль
Для понимания работы консоли изучить symfony/console библиотеку.

Кратко о том как заставить консоль симфони работать в вашем проекте:

создаем console.php в корне в нем пишем

    ```
    <?php
    $loader = require __DIR__ . '/vendor/autoload.php';
    \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader([$loader, 'loadClass']);
    
    use \Symfony\Component\Console\Application;
    
    $application = new Application();
    //данные для подключение в rabbitMQ
    $ampqConfig =  new \LHGroup\From1cToWeb\ConnectionConfig(
        "192.168.xxx.xxx",
        "5672",
        "your-user",
        "yourpassword",
        "your-vhost"
    );
    
    $application->add(
        new \LHGroup\From1cToWeb\Command\QueueConsumeCommand(
            null,
            $ampqConfig
        )
    );
    
    $application->run();
    

Запустите вашу консоль и введите php console.php и вы увидите доступные комманды

Помощь по командам можно получив дописав --help, например 

``` 
php console.php integration:publish:message --help 
```


## Пример работы скрипта:

_Пример строки по обновлению товара находится в директории doc/fixtures/productTestMessage.xml_


Запустите консоль в корне сайта и в ней

``` 
php console.php integration:consume:debug --queue=products_from_1c_to_lh
``` 
- таким образом все сообщения в очереди products_from_1c_to_lh считаются и будут обработаны или в консоли укажется ошибка по ним.

Если в очереди нет сообщений можно их добавить через команду integration:publish:message
Пример:

    ``` 
    
    php console.php integration:publish:message --queue=products_from_1c_to_lh --message='<?xml version="1.0" encoding="UTF-8" ?><root xmlns="lh:products"><item><id_erp>1234dsf</id_erp><name>Носки Falke SuperMegaDeluxNoski</name><brand>Falke</brand><type>wardrobe</type><categories><category><id_erp>32323</id_erp><name>Носки</name></category><category><id_erp>a-3232323</id_erp><name>Женские</name></category></categories><manufacturer><id_erp></id_erp><name>Falke Manufacturer</name></manufacturer></item><item><id_erp>fsd</id_erp><name>Носки2 Falke SuperMegaDeluxNoski</name><brand>Falke2</brand><type>wardrobe</type><categories><category><id_erp>32323</id_erp><name>Носки</name></category><category><id_erp>a-3232323</id_erp><name>Женские</name></category></categories><manufacturer><id_erp>12123</id_erp><name>Falke Manufacturer</name></manufacturer><specifications><specification><id_erp>1230sdf-fds</id_erp><sku>1230sdf-fds</sku><ean>1euudf-fds</ean><characteristics><characteristic><id_erp>f1fcdsfdfsf</id_erp><name>Размер</name><value>30-35</value><images><image><path>ссылка</path></image></images></characteristic><characteristic><id_erp>f1f2cdsfdfsf</id_erp><name>Цвет</name><value>Желтый</value><images><image><path>ссылка</path></image></images></characteristic></characteristics><price_base></price_base><price_discount></price_discount><price_discount_type></price_discount_type><price_erp>350</price_erp><prices><price><stock_erp_id>stock1</stock_erp_id><price_retail><price></price><discount></discount><discount_type></discount_type></price_retail><price_wholesale><price></price><discount></discount><discount_type></discount_type></price_wholesale><quantity>20</quantity></price><price><stock_erp_id>stock2</stock_erp_id><price_retail><price></price><discount></discount><discount_type></discount_type></price_retail><price_wholesale><price></price><discount></discount><discount_type></discount_type></price_wholesale><quantity>10</quantity></price></prices></specification></specifications><properties><property><id_erp>dfsdfs</id_erp><name>Ткань</name><value>Шерсть</value></property><property><id_erp>32dfsdfs</id_erp><name>Страна происхождения</name><value>Германия</value></property></properties><images><image><path>ссылка</path></image></images><width></width><height></height><depth></depth><weight></weight></item></root>'
    
    ```
где --queue - название очереди
а message - сообщение отправляемое в нее

 

## Использование на продакшине
По принципу построения консольной команды LHGroup\From1cToWeb\Command\QueueConsumeCommand нужно создать свои консольные комманды.
Они будут отличаться от текущей следующим:
- вам нужно внедрить свой сервис и логику оповещения для отправки ошибок по интеграции,
       
       ``` $notifier = new ConsoleNotify($output); ```

Этот класс должен инкапсулировать вашу логику нотификаций и сообщений об ошибках. ведь не в консоль же их выводить как это в команде integration:consume:debug реализовано. Используйте в своем классе интерфейс LHGroup\From1cToWeb\Notify\NotifyInterface

        ```  new Queue\NullProcessor() ``` 
 
 А этот должен содержать логику сохранения полученных данных из сообщения в базу. Создайте свой класс/ свою логику и используйте интерфейс LHGroup\From1cToWeb\Queue\ProcessorInterface 
